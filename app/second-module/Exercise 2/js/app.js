new Vue({
        el: '#exercise',
        data: {
            value: '',
            secondVal:''
        },
        methods: {
            alertIt: function() {
                alert("This is my alert by clicked button!");
            },
            takeVal: function(e) {
                this.value = e.target.value;
            },
            takeOther: function(e){
                this.secondVal = e.target.value;
            },
        },
    });