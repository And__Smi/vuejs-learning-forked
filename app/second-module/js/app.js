new Vue({
    el: '#app',
    data: {
        counter: 0,
        x: 0,
        y: 0,
    },
    methods: {
        increase: function(step, event) {
          this.counter += step;
        },
        changeCoords: function(e) {
            this.x = e.clientX;
            this.y = e.clientY;
        },
        dummy: function(e) {
            e.stopPropagation();
        },
        alertMe: function() {
            alert("Alert!");
        },
    },
});