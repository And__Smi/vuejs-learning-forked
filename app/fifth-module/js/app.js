new Vue({
    el: '#app',
    data:{
        goRed: false,
        goBlue: false,
        color: 'green',
        width: 100,
    },
    computed: {
      divClasses: function() {
          return {
              blue: this.goBlue,
              green: !this.goBlue
          };
      },
      myStyle: function() {
          return {
              backgroundColor: this.color,
              width: this.width + 'px',
          };
      },
    },
});